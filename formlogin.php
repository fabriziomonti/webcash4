<?php
include "webcash.inc.php";

//*****************************************************************************
class page extends webcash
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->doLogin();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* showPage
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("$this->title Login", "title");
		$this->addItem("(se hai dimenticato la password lascia il campo vuoto: sarà inviata al tuo indirizzo email)", "subtitle");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$this->form->addText("Login", "Login Utente", false, true);
		$this->form->addPassword("Password", "Password");
		
		new waLibs\waButton($this->form, 'cmd_submit', ' Login ');
		
		$this->form->getInputValues();
		}
	
	//***************************************************************************
	function doLogin()
		{
		$this->checkMandatory($this->form);
		
		$dbconn = $this->getDBConnection();
		$sql = "select Utenti.*," .
				" Casse.CiSiFida as CassaCiSiFida" .
				" from Utenti" .
				" join Casse on Utenti.IDCassa=Casse.IDCassa" .
				" where Utenti.Login=" . $dbconn->sqlString($this->form->Login) .
				" and not Utenti.Sospeso" .
				" and not Casse.Sospeso";
		$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if (!$record || $this->form->Password && $record->Password != $this->form->Password)
			{
			$this->showMessage("Utente non riconosciuto", "Login non permesso: utente non riconosciuto");
			}

		if (!$this->form->Password)
			{
			$this->sendMailPassword($record);
			$this->showMessage("Credenziali inviate", "Le credenziali sono state inviate all'indirizzo registrato per la login utente $record->Login");
			}

		$this->user = $this->record2Object($record);
		
		$this->redirect($this->startPage);
		}
			    
//*****************************************************************************
	}
	
//*****************************************************************************
// istanzia la pagina
$page = new page();
