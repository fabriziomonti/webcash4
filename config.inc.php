<?php

if (!defined('__CONFIG_VARS'))
{
		define('__CONFIG_VARS', 1);

		$__DIR__ = __DIR__;

		// file contenente i veri valori di configurazione, che non devono finire
		// su git, mentre il presente file rimane per documentazione
		@include "$__DIR__/myconfig.inc.php";

		// file contenente i parametri della versione (che cambiano e devono essere
		// inviati al server, a differenza di questi)
		include "$__DIR__/versionconfig.inc.php";

		define('APPL_DOMAIN', $_SERVER['HTTP_HOST']);
		define('APPL_DIRECTORY', '');
		define('APPL_TMP_DIRECTORY', "$__DIR__/web_files/tmp");
		define('APPL_DOC_DIRECTORY', "$__DIR__/web_files");
		define('APPL_NAME', 'webcash');
		define('APPL_TITLE', 'webCash');
		define('APPL_SMTP_SERVER', '');
		define('APPL_SMTP_USER', '');
		define('APPL_SMTP_PWD', '');
		define('APPL_SMTP_SECURE', '');
		define('APPL_SMTP_PORT', '');
		define('APPL_SUPPORT_ADDR', 'support@webappls.com');
		define('APPL_INFO_ADDR', 'info@webappls.com');
		define('APPL_SUPPORT_TEL', '051 232260');

		define("WAMODULO_EXTENSIONS_DIR", "$__DIR__/wamodulo_ext"); // directory estensioni classe modulo
		define("APPL_NOME_FILE_LOG_DB", "$__DIR__/web_files/logdb/logdb.csv"); // nome file di logging scritture su db

		set_include_path(get_include_path() . ":$__DIR__/lib");
		
		
} //  if (!defined('__CONFIG_VARS'))
