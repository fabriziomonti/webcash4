<?php
//*****************************************************************************
include "webcash.inc.php";

//*****************************************************************************
class confermaregistrazione extends webcash
	{
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		
		// apriamo il file dato con la chiave data e leggiamo l'identificativo 
		// precedentemente generato
		$IDUtente = $this->getEncryptString($_GET['k'], $_GET['f']);
		if (!$IDUtente)
			{
			$this->showMessage("Errore riferimento registrazione", "Si e' verificato un errore durante la conferma della registrazione." .
					" Assicurarsi di non avere gia' proceduto alla conferma della registrazione" .
					" effettuando un login secondo le indicazioni contenute nel messaggio email." .
					" <p>In caso contrario si prega di avvertire l'assistenza tecnica.", false, false);
			}

		$dbconn = $this->getDBConnection();
		$Sql = "SELECT Utenti.*" .
			" FROM Utenti" .
			" WHERE Utenti.IDUtente=" . $dbconn->sqlInteger($IDUtente);
		
		$user = $this->getRecordset($Sql, $dbconn, 1)->records[0];
		
		if (!$user)
			{
			$this->showMessage("Accesso non abilitato", "Accesso non abilitato", false);
			}

		if ($user->Sospeso != 1)
			{
			$this->showMessage("Registrazione gia' confermata", "La registrazione e' gia' stata confermata con successo. Usa le indicazioni" .
					" ricevute via e-mail per accedere al servizio.", false);
			}

		// riscriviamo il record affinche' non sia piu' sospeso
		$user->Sospeso = 0;
		$this->setEditorData($user);
		$this->saveRecordset($user->recordset);
		
		$this->addItem("$this->title - Conferma registrazione nuovo utente", "title");
		
		$msg =  "<div style='margin-top: 4rem;'><p>
		La registrazione per la login <b>$user->Login</b> e' stata
		confermata correttamente. Premi il bottone sottostante per accedere al
		servizio<p>&nbsp;<p>
		<form style='margin-top: 2rem; margin-bottom: 4rem;'>
		<input class='btn' type='button' value='Accedi' onclick='document.location.href=\"$this->loginPage\"'>
		</form>
		<p>&nbsp;<p>
		</div>";
		
		$this->addItem($msg, "messaggio_conferma_registrazione");
		$this->show();
		}
		
	}
	
//*****************************************************************************
// istanzia la pagina
new confermaregistrazione();
