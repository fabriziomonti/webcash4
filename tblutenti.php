<?php
//******************************************************************************
include "webcash.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends webcash
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();

		$this->addItem($this->getMenu());
		$this->addItem("Utenti", "title");
		$this->addItem($this->getTable());
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTable()
		{
		// creazione della tabella
		$dbconn = $this->getDBConnection();
		$sql = "SELECT Utenti.*," .
				" if(Capofila, 'si', 'no') as sCapofila" .
				" FROM Utenti" .
				" WHERE NOT Utenti.Sospeso" .
				" AND Utenti.IDCassa=" . $dbconn->sqlInteger($this->user->IDCassa) .
				" ORDER BY Utenti.Nome";
		
		$table = parent::getTable($sql);
		$table->formPage = "formutenti.php";
		
		// solo un capofila può manipolare gli utenti
		if (!$this->user->Capofila)
			{
			$table->removeAction("New");
			$table->removeAction("Edit");
			$table->removeAction("Delete");
			}
		
		$table->addAction("Movimenti", true);
		
		$table->addColumn("IDUtente", "ID");
		$table->addColumn("Nome", "Nome");
		$table->addColumn("Login", "Login");
		$col = $table->addColumn("EMail", "E-mail");
			$col->link = true;
		$col = $table->addColumn("Telefono", "Telefono");
		$col = $table->addColumn("sCapofila", "Capofila", true, true, true, waLibs\waTable::ALIGN_C);
			$col->aliasOf = "IF(Capofila, 'si', 'no')";
		$col = $table->addColumn("Saldo", "Saldo");
			
		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$table->loadRows())
			{
			$this->showDBError($table->recordset->dbConnection);
			}

		return $table;
		}

	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();