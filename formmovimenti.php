<?php
//*****************************************************************************
include "webcash.inc.php";

//*****************************************************************************
class page extends webcash
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct();
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		elseif ($this->form->isToDelete())
			{
			$this->deleteRecord($this->form->record);
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$context = $this->getTitleContext("Utenti", "IDUtente");
		$this->addItem("Scheda movimento $context", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$dbconn = $this->form->recordset->dbConnection;
		$readOnly = false;

		if (!$_GET["IDUtente"])
			{
			$myReadOnly = !$this->user->CassaCiSiFida;
			$ctrl = $this->form->addSelect("IDUtente", "Utente", $myReadOnly, !$myReadOnly);
				$ctrl->sql = "select IDUtente, Nome from Utenti where not Sospeso" .
							" and IDCassa=" . $dbconn->sqlInteger($this->user->IDCassa) .
							" order by nome";
				$ctrl->value = $this->user->IDUtente;
			}
			
		$this->form->addSelect("IDTipoMovimento", "Tipo", $readOnly, true)
			->sql = "select IDTipoMovimento, DescrizioneTipoMovimento from TipiMovimenti where not Sospeso" .
						" order by IDTipoMovimento";
		
		$this->form->addText("Causale", "Causale", true);
		
		$ctrl = $this->form->addSelect("IDUtenteBeneficiario", "Beneficiario", true);
			$ctrl->sql = "select IDUtente, DescrizioneTipoMovimento from Utenti where not Sospeso" .
							" and IDCassa=" . $dbconn->sqlInteger($this->user->IDCassa) .
							" order by nome";
		
		$this->form->addCurrency("ImportoMovimento", "Importo", $readOnly, true);
		$this->form->addDateTime("DataOraMovimento", "Data/Ora movimento", $readOnly, true)->value = time();
		
		$ctrl = $this->form->addUpload("Documento", "Documento", $readOnly);
			$this->setUrlDoc($ctrl);
		
		$this->form->addTextArea("NoteMovimento", "Note", $readOnly);
		$this->form_submitButtons($this->form, $readOnly, false);

		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "SELECT *" .
				" FROM Movimenti" .
				" WHERE IDMovimento=" . $dbconn->sqlInteger($_GET['IDMovimento']) . 
				" AND NOT Sospeso";
			
		$recordset = $this->getRecordset($sql, $dbconn, $_GET['IDMovimento'] ? 1 : 0);
		if ($_GET['IDMovimento'] && !$recordset->records)
			{
			$this->showMessage("Record non trovato", "Record non trovato", false, true);
			}

		if (!$this->isRecordDeletable($recordset->records[0]))
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa");
			}
			
		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);
		
		$record = $this->form->recordset->records[0];
		if (!$record)
			{
			if (!$this->user->CassaCiSiFida  && $this->form->IDUtente)
				{
				$this->showMessage("Operazione non permessa", "Operazione non permessa");
				}
			$record = $this->form->recordset->add();
			$record->IDUtente = !$this->user->CassaCiSiFida ? $this->user->IDUtente : $_GET["IDUtente"];
			$record->DataOraRegistrazione = time();
			}
		else 
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa");
			}
			
		$dbconn = $this->form->recordset->dbConnection;
		
		$dbconn->beginTransaction();
		$this->form->save();
		$this->setEditorData($record);
		$this->saveRecordset($record->recordset);
		$insertId = $dbconn->lastInsertedId();
		$this->saveDoc($this->form->inputControls["Documento"]);
		
		if ($record->IDTipoMovimento == APPL_TIPO_MOV_SPESA)
			{
			$this->updateSaldiSpesa($record);
			}
		elseif ($record->IDTipoMovimento == APPL_TIPO_MOV_VERSAMENTO)
			{
			$this->updateSaldiVersamento($record);
			}
			
		$dbconn->commitTransaction();
		
		$retValues = $insertId ? array_merge(array("insertId" => $insertId), $this->form->input) : $this->form->input;
		$this->response($retValues);
		}
		
	//***************************************************************************
	function deleteRecord()
		{
		$record = $this->form->recordset->records[0];
		$dbconn = $this->form->recordset->dbConnection;
		$dbconn->beginTransaction();
		parent::deleteRecord($record, false);
		if ($record->IDTipoMovimento == APPL_TIPO_MOV_SPESA)
			{
			$this->updateSaldiSpesa($record);
			}
		elseif ($record->IDTipoMovimento == APPL_TIPO_MOV_VERSAMENTO)
			{
			$this->updateSaldiVersamento($record);
			}
			
		$dbconn->commitTransaction();
		$this->response();
		}
		
	//*****************************************************************************
	function isRecordDeletable(waLibs\waRecord $record = null)
		{
		if (!$record)
			{
			return true;
			}
			
		return $this->user->CassaCiSiFida || $record->IDUtente == $this->user->IDUtente;
		}
		
	//***************************************************************************
	function updateSaldiVersamento(waLibs\waRecord $movimento)
		{
		// se e' un versamento, il saldo di chi ha effettuato il versamento
		// si alza dell'intero importo e il saldo del beneficiario
		// si abbassa dell'intero importo
		$dbconn = $movimento->recordset->dbConnection;
		$ImportoMovimento = $movimento->ImportoMovimento * ($movimento->Sospeso ? -1 : 1);
		$sql = "UPDATE Utenti SET Saldo=(Saldo +  " . $dbconn->sqlDecimal($ImportoMovimento) . ")," .
				$this->getSqlEditorData($dbconn) . 
				" WHERE IDUtente=" . $dbconn->sqlInteger($movimento->IDUtente);
		$this->dbExecute($sql, $dbconn);
		$sql = "UPDATE Utenti SET Saldo=(Saldo - " . $dbconn->sqlDecimal($ImportoMovimento) . ")," .
				$this->getSqlEditorData($dbconn) . 
				" WHERE IDUtente=" . $dbconn->sqlInteger($movimento->IDUtenteBeneficiario);
		$this->dbExecute($sql, $dbconn);
		}
		
	//***************************************************************************
	function updateSaldiSpesa(waLibs\waRecord $movimento)
		{
		$dbconn = $movimento->recordset->dbConnection;
		$sql = "SELECT *" .
				" FROM Utenti" .
				" WHERE IDCassa=" . $this->user->IDCassa .
				" AND DataOraCreazione<" . $dbconn->sqlDateTime($movimento->DataOraRegistrazione) .
				" AND NOT Sospeso" .
				" order by if(IDUtente=" . $dbconn->sqlInteger($movimento->IDUtente) . ", 0, 1)";
		$utenti = $this->getRecordset($sql, $dbconn);
		
		$nrQuote = count($utenti->records);
		
		$ImportoMovimento = $movimento->ImportoMovimento * ($movimento->Sospeso ? -1 : 1);
		$quota = round($ImportoMovimento / $nrQuote, 6, PHP_ROUND_HALF_DOWN);

		$parziale = 0;
		foreach ($utenti->records as $idx => $utente)
			{
			if ($idx == count($utenti->records) - 1)
				{
				// l'ultima quota, a casaccio, si becca anche i rimasugli per
				// fare quadrare i conti...
				$quota = $ImportoMovimento - $parziale;
				}
				
			if ($utente->IDUtente == $movimento->IDUtente)
				{
				$utente->Saldo += $ImportoMovimento - $quota;
				}
			else
				{
				$utente->Saldo -= $quota;
				}
				
			$this->setEditorData($utente);
			$parziale += $quota;
			}
		
		$utenti->save();
		}
		
	//*****************************************************************************
	function rpc_reloadBeneficiari($IDUtenteVersante)
		{
		$dbconn = $this->form->recordset->dbConnection;
				
		$sql = "SELECT IDUtente,Nome" .
				" FROM Utenti" .
				" WHERE not Sospeso" .
				" AND Utenti.IDCassa=" . $dbconn->sqlInteger($this->user->IDCassa) .
				" AND Utenti.IDUtente!=" . $dbconn->sqlInteger($IDUtenteVersante) . 
				" ORDER BY Nome";
		$rs = $this->getRecordset($sql, $dbconn);
		$retval = array();
		foreach ($rs->records as $record)
			{
			$retval[$record->IDUtente] = $record->Nome;
			}
		return $retval;
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
