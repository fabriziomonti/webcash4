<?php
//******************************************************************************
include "webcash.inc.php";

//******************************************************************************
/**
 */
//******************************************************************************
class page extends webcash
	{
		
	//*****************************************************************************
	function __construct()
		{
		parent::__construct();

		$this->childWindow = (boolean) $_GET["IDUtente"];
		$this->addItem($this->getMenu());
		
		$context = $this->getTitleContext("Utenti", "IDUtente");
		$this->addItem("Movimenti $context", "title");
		$this->addItem($this->getTable());
		$this->show();
		}

	//*****************************************************************************
	/**
	 * @return waLibs\waTable
	 */
	function getTable()
		{
		// creazione della tabella
		$dbconn = $this->getDBConnection();
		$sql = "SELECT Movimenti.*," .
				" 'http://www.repubblica.it' as URLDocumento," .
				" TipiMovimenti.DescrizioneTipoMovimento," .
				" Utenti.Nome as NomeUtente," .
				" UtentiBeneficiari.Nome AS NomeUtenteBeneficiario" .
				" FROM Movimenti" .
				" JOIN Utenti ON Movimenti.IDUtente=Utenti.IDUtente" .
				" JOIN TipiMovimenti ON Movimenti.IDTipoMovimento=TipiMovimenti.IDTipoMovimento" .
				" LEFT JOIN Utenti AS UtentiBeneficiari ON Movimenti.IDUtenteBeneficiario=UtentiBeneficiari.IDUtente" .
				" WHERE NOT Movimenti.Sospeso" .
				" AND Utenti.IDCassa=" . $dbconn->sqlInteger($this->user->IDCassa) .
				($_GET["IDUtente"] ? " and Movimenti.IDUtente=" . $dbconn->sqlInteger($_GET["IDUtente"]) : '') .
				" ORDER BY Movimenti.DataOraRegistrazione DESC";
		
		$table = parent::getTable($sql);
		$table->formPage = "formmovimenti.php";
		
		$table->removeAction("Edit");
		if ($_GET["IDUtente"] && $_GET["IDUtente"] != $this->user->IDUtente && !$this->user->CassaCiSiFida)
			{
			$table->removeAction("New");
			}
		$table->actions["Delete"]->enablingFunction = array($this, "isRecordDeletable");
		
		$table->addColumn("IDMovimento", "ID")->aliasOf = "Movimenti.IDMovimento";
		if (!$_GET["IDUtente"])
			{
			$table->addColumn("NomeUtente", "Utente")->aliasOf = "Utenti.Nome";
			}
			
		$col = $table->addColumn("DataOraMovimento", "Data");
			$col->aliasOf = "Movimenti.DataOraMovimento";
			$col->format = waLibs\waTable::FMT_DATE;
		$table->addColumn("DescrizioneTipoMovimento", "Tipo")->aliasOf = "TipiMovimenti.DescrizioneTipoMovimento";
		$col = $table->addColumn("ImportoMovimento", "Importo");
			$col->aliasOf = "Movimenti.ImportoMovimento";
			$col->totalize = true;
		$table->addColumn("Causale", "Causale")->aliasOf = "Movimenti.Causale";
		$table->addColumn("NomeUtenteBeneficiario", "Beneficiario")->aliasOf = "UtentiBeneficiari.Nome";
		$col = $table->addColumn("DataOraRegistrazione", "Data reg.");
			$col->aliasOf = "Movimenti.DataOraRegistrazione";
			$col->format = waLibs\waTable::FMT_DATE;
		$col = $table->addColumn("Documento", "Documento");
			$col->aliasOf = "Movimenti.Documento";
			$col->link = true;
		$table->addColumn("NoteMovimento", "Note")->aliasOf = "Movimenti.NoteMovimento";

		// colonne invisibili
		$table->addColumn("URLDocumento", "URLDocumento", false, false, false)
			->computeFunction = array($this, "getUrlDoc");
		
		// lettura dal database delle righe che andranno a popolare la tabella
		if (!$table->loadRows())
			{
			$this->showDBError($table->recordset->dbConnection);
			}

		return $table;
		}

	//*****************************************************************************
	function isRecordDeletable(waLibs\waTable $table)
		{
		return $this->user->CassaCiSiFida || 
				$table->record->IDUtente == $this->user->IDUtente;
		}
		
	//*****************************************************************************
	function getUrlDoc(waLibs\waTable $table)
		{
		return parent::getUrlDoc($table->record, "Documento");
		}
		
	//*****************************************************************************
	}

// fine classe pagina
//*****************************************************************************
// istanzia la pagina
new page();