<?php
include "webcash.inc.php";

//*****************************************************************************
class page extends webcash
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true, true);
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Configurazione cassa", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$record = $this->form->recordset->records[0];
		$readOnly = false;
		
		$this->form->addBoolean("CiSiFida", "Ci fidiamo?", $readOnly);

		$this->form_submitButtons($this->form, false, false);
		
		$this->form->getInputValues();

		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select Casse.*" .
				" from Casse" .
				" where Casse.IDCassa=" . $dbconn->sqlInteger($this->user->IDCassa);
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		if (!$recordset->records)
			{
			$this->showMessage("Item not found", "Item not found", false, true);
			}

		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		$this->checkMandatory($this->form);
		
		$record = $this->form->recordset->records[0];
			
		$this->form->save();
		$this->setEditorData($record);
		$this->saveRecordset($record->recordset);
		
		$this->user->CassaCiSiFida = $this->form->CiSiFida;
		
		$this->response();
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
