<?php
//*****************************************************************************
include "webcash.inc.php";

//*****************************************************************************
class page extends webcash
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true, true);
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		elseif ($this->form->isToDelete())
			{
			$this->deleteRecord($this->form->record);
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Scheda utente", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$record = $this->form->recordset->records[0];
		$readOnly = false;

		$this->form->addText("Login", "Login Utente", $readOnly, true);
		$this->form->addText("Nome", "Nome", $readOnly, true);
		$this->form->addEmail("EMail", "E-Mail", $readOnly, true);
		$this->form->addText("Telefono", "Telefono", $readOnly);
		
		$this->form->addTextArea("NoteUtente", "Note", $readOnly);
		$this->form->addBoolean("invia_credenziali", "Spedisci credenziali via email", $readOnly);
		$this->form_submitButtons($this->form, $readOnly, $record ? true : false);

		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "SELECT *" .
				" FROM Utenti" .
				" WHERE IDUtente=" . $dbconn->sqlInteger($_GET['IDUtente']) . 
				" AND NOT Sospeso";
			
		$recordset = $this->getRecordset($sql, $dbconn, $_GET['IDUtente'] ? 1 : 0);
		if ($_GET['IDUtente'] && !$recordset->records)
			{
			$this->showMessage("Record non trovato", "Record non trovato", false, true);
			}

		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);
		
		$record = $this->form->recordset->records[0];
		if (!$record)
			{
			$record = $this->form->recordset->add();
			$record->IDCassa = $this->user->IDCassa;
			$record->Password = $this->getPassword();
			$record->DataOraCreazione = time();
			$record->FirstEMail = $this->form->EMmail;
			}
		else 
			{
			$this->checkLockViolation($this->form);
			}
			
		$dbconn = $this->form->recordset->dbConnection;
		
		// verifichiamo che la login non sia già esistente
		$this->checkLoginExists($this->form->Login, $record->IDUtente, $dbconn);
		
		$this->form->save();

		$this->setEditorData($record);
		$this->saveRecordset($record->recordset);
		
		$insertId = $dbconn->lastInsertedId();
		if ($this->form->invia_credenziali)
			{
			$this->sendMailPassword($record);
			}

		$retValues = $insertId ? array_merge(array("insertId" => $insertId), $this->form->input) : $this->form->input;
		$this->response($retValues);
		}
		
	//***************************************************************************
	function rpc_checkloginExists($Login)
		{
		return $this->checkLoginExists($Login, $this->form->recordset->records[0]->IDUtente, $this->form->recordset->dbConnection, false);
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
