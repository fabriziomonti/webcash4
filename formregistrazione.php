<?php
//*****************************************************************************
include "webcash.inc.php";

//*****************************************************************************
class page extends webcash
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
	
	//**************************************************************************
	function __construct()
		{
		parent::__construct(false);
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("$this->title - Scheda registrazione nuovo utente", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$readOnly = false;

		$this->form->addText("Login", "Login Utente", $readOnly, true);
		$this->form->addEmail("EMail", "E-Mail", $readOnly, true);
		$this->form->addText("Nome", "Nome", $readOnly, true);
		
		$this->form->addTextArea("Condizioni", "Condizioni del servizio", true)
			->value = file_get_contents("condizioni.txt");
		
		$this->form->addBoolean("ChkCondizioni", "Ho letto le condizioni", false, true);

		$this->form->addTextArea("Testo196", "Informativa trattamento dati", true)
			->value = file_get_contents("196.txt");
		
		$this->form->addBoolean("ChkInformativa", "Ho letto l'informativa", false, true);
		
		$this->form->addCaptcha("Captcha", "Codice di controllo", false, true);

		new waLibs\waButton($this->form, 'cmd_submit', "Registrami");
		$ctrl = new waLibs\waButton($this->form, 'cmd_help', "Help");
			$ctrl->submit = false;

		$this->form->getInputValues();
		}

	//***************************************************************************
	/**
	* -
	*
	* @return waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "SELECT * FROM Utenti";
			
		$recordset = $this->getRecordset($sql, $dbconn, 0);
		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		// controlli obbligatorieta' e formali
		$this->checkMandatory($this->form);

		$dbconn = $this->form->recordset->dbConnection;
		
		// verifichiamo che la login non sia già esistente
		$this->checkLoginExists($this->form->Login, 0, $dbconn);

		$dbconn->beginTransaction();
		$cash = $this->createCash($dbconn);
		$user = $this->createUser($cash);
		$this->mandaMail($user);
		$dbconn->commitTransaction();
		
		$msg =  "La prima fase della registrazione e' avvenuta correttamente. " .
				"Riceverai un messaggio di posta elettronica all'indirizzo da te" . 
				" indicato (<b>$user->EMail</b>): segui le indicazioni contenute nel " .
				"messaggio per completare la registrazione.";
	
		$this->showMessage("Registrazione effettuata", $msg, false);
		}
		
	//***************************************************************************
	function createCash(waLibs\waDBConnection $dbconn)
		{
		$sql = "select * from Casse";
		$cash = $this->getRecordset($sql, $dbconn, 0)->add();
		$cash->CiSiFida = 1;
		$this->setEditorData($cash);
		$this->saveRecordset($cash->recordset);

		return $cash;
		}
	
	//***************************************************************************
	function createUser(waLibs\waRecord $cash)
		{
		$user = $this->form->recordset->add();

		$user->IDCassa = $cash->IDCassa;
		$user->Password = $this->getPassword();
		$user->DataOraCreazione = time();
		$user->Login = $this->form->Login;
		$user->Nome = $this->form->Nome;
		$user->EMail = $this->form->EMail;
		$user->FirstEMail = $this->form->EMail;
		$user->Capofila = 1;
		$user->Sospeso = 1;

		$this->setEditorData($user);
		$this->saveRecordset($user->recordset);

		return $user;
		}
	
	//***************************************************************************
	function mandaMail(waLibs\waRecord $user)
		{
		// salviamo l'id dentro un file con chiave e spediamo all'utente la chiave
		// (se gli mandassimo solo l'id sarebbe troppo facile aggirare la
		// one-time-password...)
		$esito = $this->saveEncryptString($user->IDUtente);	
		if (!is_array($esito))
			{
			$this->showMessage("Errore generazione messaggio email", "Si e' verificato un errore durante la generazione del messaggio di posta elettronica" .
					" da inviare all'indirizzo $user->EMail. Si prega di riprovare piu' tardi.");
			}
		list($key, $filename) = $esito;
		$filename = basename($filename);
		
		$body = "Ciao $user->Nome,\r\n\r\n" .
				"io sono una risposta automatica generata dal servizio " . 
				"$this->title.\r\n\r\n" .
				"Hai inviato una richiesta di registrazione a $this->title? " .
				"Se si', la tua registrazione deve essere confermata tramite la seguente pagina web:\r\n\r\n" .
				"$this->protocol://$this->domain$this->httpwd/confermaregistrazione.php?k=$key&f=$filename\r\n\r\n" .
				
				"Questi sono le credenziali e gli indirizzi che, una volta confermata la registrazione tramite il link soprastante," .
				" potrai utilizzare per accedere alle funzionalita' del servizio:\r\n\r\n" .
				"- pagina del servizio: $this->protocol://$this->domain$this->httpwd\r\n" .
				"- login utente:        $user->Login\r\n" . 
				"- password:            $user->Password (potrai cambiarla ogni volta che vorrai nella pagina delle tue Preferenze).\r\n\r\n" . 
				
				"Qualora invece tu non avessi effettuato alcuna richiesta di registrazione, ti preghiamo di scusarci dell'intrusione: " .
				"qualche buontempone ci ha fatto uno scherzo di pessimo gusto, usando sconsideratamente il tuo indirizzo di posta elettronica." .
				" In questo caso, non devi fare assolutamente nulla: se non confermata, la registrazione non avra' alcun effetto.\r\n\r\n" .
				"Ciao da $this->title ($this->protocol://$this->domain).";
	
		if (!$this->sendMail($user->EMail, "$this->title - conferma registrazione", $body))
			{
			$this->showMessage("Errore invio messaggio email", "Si e' verificato un errore durante l'invio del messaggio di posta elettronica" .
					" all'indirizzo $user->EMail. Sei pregato di riprovare piu' tardi.", false, false);
			}
		}
	
	//***************************************************************************
	function rpc_checkloginExists($Login)
		{
		return $this->checkLoginExists($Login, 0, null, false);
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
