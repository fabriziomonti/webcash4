<?php
include "webcash.inc.php";

//*****************************************************************************
class page extends webcash
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct();
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Profilo", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		$record = $this->form->recordset->records[0];
		$readOnly = false;
		
		$this->form->addText("Nome", "Nome", $readOnly, true);
		$this->form->addEmail("EMail", "Email", $readOnly, true);
		$this->form->addText("Telefono", "Telefono", $readOnly);
		
		$this->form->addText("Login", "Login Utente", $readOnly, true);
		$this->form->addPassword("Password", "Password", $readOnly, true);
		$ctrl = $this->form->addPassword("ConfirmPassword", "Conferma password", $readOnly, true);
			$ctrl->dbBound = false;
			$ctrl->value = $record->Password;
			$ctrl->maxChars = 10;

		$this->form_submitButtons($this->form, false, false);
		
		$this->form->getInputValues();

		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select Utenti.*" .
				" from Utenti" .
				" where Utenti.IDUtente=" . $dbconn->sqlInteger($this->user->IDUtente);
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		if (!$recordset->records)
			{
			$this->showMessage("Item not found", "Item not found", false, true);
			}

		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		$this->checkMandatory($this->form);
		
		$record = $this->form->recordset->records[0];
		if (!$this->form->Password || ($this->form->Password != $this->form->ConfirmPassword))
			{
			$this->showMessage("Password non corrispondenti", "Password non corrispondenti");
			}
			
		// verifichiamo che la login non sia già esistente
		$this->checkLoginExists($this->form->Login, $this->user->IDUtente, $record->recordset->dbConnection);
			
		$this->form->save();
		$this->setEditorData($record);
		$this->saveRecordset($record->recordset);
		$this->user = $this->record2Object($record);
		
		$this->response();
		}
		
	//***************************************************************************
	function rpc_checkloginExists($Login)
		{
		return $this->checkLoginExists($Login, $this->user->IDUtente, $this->form->recordset->dbConnection, false);
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
