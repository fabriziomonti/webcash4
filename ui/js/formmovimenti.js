//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
// classe waPage
var waPage = new Class
(
	{
	//-------------------------------------------------------------------------
	// extends
	Extends: webcash,

	//-------------------------------------------------------------------------
	// proprieta'

	//-------------------------------------------------------------------------
	//initialization  
	// la fa il parent
	
	//-------------------------------------------------------------------------
	event_onchange_waForm_IDUtente: function()
		{
		this.event_onchange_waForm_IDTipoMovimento();	
		},
		
	//-------------------------------------------------------------------------
	event_onchange_waForm_IDTipoMovimento: function()
		{
		var IDTipoMovimento = this.form.controls.IDTipoMovimento;
		var IDUtenteBeneficiario = this.form.controls.IDUtenteBeneficiario;
		var Causale = this.form.controls.Causale;

		IDUtenteBeneficiario.empty();
		if (IDTipoMovimento.get() == 2)
			{
			var list = this.form.RPC("rpc_reloadBeneficiari", this.form.controls.IDUtente.get());
			var toSelect = '';
			if (this.dictionaryLength(list)  == 1)
				{
				for (var k in list) 
					{
					toSelect = k;
					break;
					}
				}
			IDUtenteBeneficiario.fill(list, toSelect);
			IDUtenteBeneficiario.enable(true, true);
			IDUtenteBeneficiario.setMandatory(true, true);
			Causale.enable(false, true);
			Causale.setMandatory(false, true);
			}
		else
			{
			IDUtenteBeneficiario.enable(false, true);
			IDUtenteBeneficiario.setMandatory(false, true);
			Causale.enable(true, true);
			Causale.setMandatory(true, true);
			}
	
		}

	
	}
);

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
document.waPage = new waPage();
