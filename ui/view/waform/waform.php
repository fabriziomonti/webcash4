<?php 
namespace webcash;
	
//******************************************************************************
class waFormView implements \waLibs\i_waFormView
	{
	/**
	 * dati della form
	 * @var \waLibs\waFormData  
	 */
	protected $data = null;
	
	//**************************************************************************
	public function transform(\waLibs\waFormData $data)
		{
		$this->data = $data;
		
		// i parametri sono anche trasformati in proprietà locali
		foreach ($data as $key => $val)
			{
			$this->$key = $val;
			}

		$this->setCssLink();
		$this->setJavascriptLink();
		$this->setForm();
		$this->setJavascriptObjects();
		
		}
		
	//**************************************************************************
	public function transformInput(\waLibs\waFormData $data)
		{
		//	loop dei controlli 
		foreach ($data->controls as & $control)
			{
			require_once __DIR__ . "/$control->type.php";
			$className = __NAMESPACE__ . "\\wa" . ucfirst($control->type) . "View";
			$view = new $className($data);
			$control->inputValue = $view->transformInput($control);
			}
		
		return $data;
		}
		
	//**************************************************************************
	private function setCssLink()
		{
		?>
		<!-- Bootstrap core CSS -->
		<link href='//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css' rel='stylesheet'>

		<link href='<?=$this->waFormPath?>/uis/waform_default/css/waform.css' rel='stylesheet'/>
		<?php
		}
		
	//**************************************************************************
	private function setJavascriptLink()
		{
		?>
		<!-- Bootstrap core JavaScript
		================================================== -->

		<!--gestione date-->
		<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/moment-with-locales.min.js'></script>
		<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js'></script>
		<!--spinner (loader)-->
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/spin.js/2.3.2/spin.min.js"></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/spin_extension.js'></script>
		
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/form.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/control.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/text.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/select.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/boolean.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/button.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/captcha.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/currency.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/date.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/datetime.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/email.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/frame.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/integer.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/label.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/multiselect.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/selecttypeahead.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/notcontrol.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/option.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/password.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/taxcode.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/textarea.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/time.js'></script>
		<script type='text/javascript' src='<?=$this->waFormPath?>/uis/waform_default/js/upload.js'></script>
		
		<?php
		}
		
	//**************************************************************************
	private function setForm()
		{

		?>

		<form 
			id='<?=$this->name?>' 
			action='<?=$this->destinationPage?>' 
			method='post' 
			enctype='multipart/form-data' 
			class='waform' 
			style=''
			onsubmit='return false'
		>
			<div class='container'>
				<!-- controllo hidden che viene utilizzato dalla classe per stabilire 
				se la form che ha effettuato submit sia relativa alla propria 
				istanza o meno -->
				<input type='hidden' name='waform_form_name' value='<?=$this->name?>' />

				<!-- controllo hidden da valorizzare via js a seconda del tipo di bottone 
				che e' stato usato per submit; se l'applicazione deve gestire anche il nojs,
				allora per verificare che tipo di operazione l'utente ha scelto occorre
				basarsi sul nome del bottone premuto
				default=aggiornamento -->
				<input type='hidden' name='waform_action' value='<?=\waLibs\waForm::ACTION_EDIT?>' />

				<!-- evntuale recid se qualcuno lo vuole utilizzare -->
				<?php
				if ($this->recId->value)
					{
					?>
					<input type='hidden' name='<?=$this->recId->name?>' value='<?=$this->recId->value?>' />
					<?php
					}
				?>
					
				<!--		mod id per check violation-->
				<?php
				if ($this->mdId->value)
					{
					?>
					<input type='hidden' name='<?=$this->mdId->name?>' value='<?=$this->mdId->value?>' />
					<?php
					}

				//	loop dei controlli 
				foreach ($this->controls as $idx => $control)
					{
					require_once __DIR__ . "/$control->type.php";
					$className = __NAMESPACE__ . "\\wa" . ucfirst($control->type) . "View";
					$view = new $className($this->data);
					$view->transform($control);
					}

				?>
				

			</div>
		</form>	
		
		<?php
		}
		
	//**************************************************************************
	private function setJavascriptObjects()
		{
		
		?>
		
		<script type='text/javascript'>
			document.<?=$this->name?> = new waForm('<?=$this->name?>');
			
			<?php
			foreach ($this->controls as $control)
				{
				$value = is_array($control->value) ? "" : $this->escapeValuesForJavascript($control->value)
				?>

				new <?=$control->class?> (document.<?=$this->name?>, "<?=$control->name?>", "<?=$value?>", '<?=$control->visible?>', '<?=$control->readOnly?>', '<?=$control->mandatory?>');
				
				<?php
				}
			?>
				
		</script>		
		
		<?php
		}
		
	//**************************************************************************
	private function escapeValuesForJavascript($value)
		{
		$value = str_replace("\t", "\\t", $value);
		$value = str_replace("\r", "\\r", $value);
		$value = str_replace("\n", "\\n", $value);
		return htmlspecialchars($value, ENT_QUOTES |  ENT_HTML5);
		}
		
		
	//**************************************************************************
	}
//******************************************************************************


