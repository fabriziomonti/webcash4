SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS Casse (
IDCassa int(10) NOT NULL,
  CiSiFida int(1) NOT NULL DEFAULT '0',
  NoteCassa text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT NULL,
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS `Help` (
IDHelp int(10) NOT NULL,
  Pagina varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  Filtro varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  IDOperazione int(10) NOT NULL DEFAULT '0',
  NomeCampo varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  Testo longtext COLLATE latin1_general_ci NOT NULL,
  NoteHelp text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT NULL,
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS Movimenti (
IDMovimento int(10) NOT NULL,
  IDUtente int(10) NOT NULL DEFAULT '0',
  IDTipoMovimento int(10) NOT NULL DEFAULT '0',
  ImportoMovimento decimal(10,6) NOT NULL DEFAULT '0.000000',
  DataOraMovimento datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  Causale varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  IDUtenteBeneficiario int(10) DEFAULT NULL,
  IDAutoreMovimento int(10) NOT NULL DEFAULT '0',
  DataOraRegistrazione datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  Documento varchar(50) COLLATE latin1_general_ci DEFAULT NULL,
  NoteMovimento text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT NULL,
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS Operazioni (
  IDOperazione int(10) NOT NULL DEFAULT '0',
  DescrizioneOperazione varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  NoteOperazione text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT '0000-00-00 00:00:00',
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS TipiMovimenti (
  IDTipoMovimento int(10) NOT NULL DEFAULT '0',
  DescrizioneTipoMovimento varchar(40) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  NoteTipoMovimento text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT '0000-00-00 00:00:00',
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

CREATE TABLE IF NOT EXISTS Utenti (
IDUtente int(10) NOT NULL,
  IDCassa int(10) NOT NULL DEFAULT '0',
  Capofila int(1) NOT NULL DEFAULT '0',
  Nome varchar(50) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  Login varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `Password` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  Telefono varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  EMail varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  Saldo decimal(10,6) NOT NULL DEFAULT '0.000000',
  FirstEMail varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  DataOraCreazione datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  NoteUtente text COLLATE latin1_general_ci,
  Sospeso int(1) DEFAULT '0',
  DataOraUltimaModifica datetime DEFAULT NULL,
  IPUltimaModifica varchar(20) COLLATE latin1_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;


ALTER TABLE Casse
 ADD PRIMARY KEY (IDCassa);

ALTER TABLE Help
 ADD PRIMARY KEY (IDHelp), ADD KEY URI (Pagina), ADD KEY NomeCampo (NomeCampo);

ALTER TABLE Movimenti
 ADD PRIMARY KEY (IDMovimento);

ALTER TABLE Operazioni
 ADD PRIMARY KEY (IDOperazione);

ALTER TABLE TipiMovimenti
 ADD PRIMARY KEY (IDTipoMovimento);

ALTER TABLE Utenti
 ADD PRIMARY KEY (IDUtente);


ALTER TABLE Casse
MODIFY IDCassa int(10) NOT NULL AUTO_INCREMENT;
ALTER TABLE Help
MODIFY IDHelp int(10) NOT NULL AUTO_INCREMENT;
ALTER TABLE Movimenti
MODIFY IDMovimento int(10) NOT NULL AUTO_INCREMENT;
ALTER TABLE Utenti
MODIFY IDUtente int(10) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
