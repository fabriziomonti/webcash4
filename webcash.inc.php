<?php
//****************************************************************************************
include_once (__DIR__ . "/defines.inc.php");
include_once (__DIR__ . "/config.inc.php");
include_once (__DIR__ . "/walibs4/waapplication/waapplication.class.php");


//****************************************************************************************
class webcash extends waLibs\waApplication
	{
	var $directoryDoc;
	var $webDirectoryDoc;
	var $sessionData;
	var $fileConfigDB = '';
	
	// dati di editing da appiccicare a tutte le modifiche sulle tabelle del db
	var $formModId = "DataOraUltimaModifica";
	var $formModIp = "IPUltimaModifica";
	
	/**
	* contiene i dati dell'operatore loggato alla sessione
	*/
	var $user = array();

	var $userPreferences;
	
	var $startPage = 'tblmovimenti.php';
	var $loginPage = 'formlogin.php';
	
	// indica se stiamo facendo vedere qualcosa in una finestra figlia
	var $childWindow = false;
	
	// protocollo utlizzato (http/s)
	var $protocol = "https";
	
	public static $applicationInstance;
	
	//****************************************************************************************
	/**
	* costruttore
	*
	*/
	function __construct($checkUser = true, $checkAdmin = false)
		{
		$this->useSession = true;
		$this->domain = APPL_DOMAIN;
		$this->httpwd = APPL_DIRECTORY;
		$this->directoryTmp = APPL_TMP_DIRECTORY;
		$this->name = APPL_NAME;
		$this->title = APPL_TITLE;
		$this->version = APPL_REL;
		$this->versionDate = APPL_REL_DATE;
		$this->smtpServer = APPL_SMTP_SERVER;
		$this->smtpUser = APPL_SMTP_USER;
		$this->smtpPassword = APPL_SMTP_PWD;
		$this->smtpSecurity = APPL_SMTP_SECURE;
		$this->smtpPort = APPL_SMTP_PORT;
		$this->supportEmail = APPL_SUPPORT_ADDR;
		$this->infoEmail = APPL_INFO_ADDR;
		
		$this->directoryDoc = APPL_DOC_DIRECTORY;
		$this->webDirectoryDoc = APPL_WEB_DOC_DIRECTORY;
		$this->protocol = $this->getProtocol();

		$this->fileConfigDB = __DIR__ . "/dbconfig.inc.php";
		
		self::$applicationInstance = $this;
		
		$this->init();
		
		$this->sessionData = &$_SESSION[$this->name];
		$this->user = &$this->sessionData['user'];
		
		// impostazioni delle preferenze
        $this->userPreferences = unserialize(base64_decode($_COOKIE[$this->name . "_prefs"]));
		$this->userPreferences["navigation_by_windows"] = isset($this->userPreferences["navigation_by_windows"]) ? $this->userPreferences["navigation_by_windows"] : false;
		$this->userPreferences["max_table_rows"] = $this->userPreferences["max_table_rows"] ? $this->userPreferences["max_table_rows"] : waLibs\waTable::LIST_MAX_REC;		
		$this->userPreferences["table_actions"] = isset($this->userPreferences["table_actions"]) ? $this->userPreferences["table_actions"] : "watable_actions_context";
		$this->navigationMode = $this->userPreferences["navigation_by_windows"] ? waLibs\waApplication::NAV_WINDOW: waLibs\waApplication::NAV_INNER;

		// verifica validità dell'utent loggato
		$this->checkUser($checkUser, $checkAdmin);
			
		}
	
	//*****************************************************************************
	/**
	* a seconda dello stato della form, chiama l'opportuno metodo
	*
	* @return waConnessiondDB
	*/
	function getDBConnection($fileConfigDB = '')
		{
		return parent::getDBConnection($fileConfigDB ? $fileConfigDB : $this->fileConfigDB);
		}
		
	//*****************************************************************************
	function record2Array(waLibs\waRecord $record)
		{
		$retval = array();
		$rs = $record->recordset;
		for ($i = 0; $i < $rs->fieldNr(); $i++)
			{
			$retval[$rs->fieldName($i)] = $record->$i;
			}
		
		return $retval;
		}
		
	//*****************************************************************************
	function setEditorData(waLibs\waRecord $record)
		{
		$record->insertValue($this->formModId, time());
		$record->insertValue($this->formModIp, $_SERVER['REMOTE_ADDR']);
		}

	//*****************************************************************************
	function getSqlEditorData(waLibs\waDBConnection $dbconn)
		{
		return " $this->formModId=" . $dbconn->sqlDateTime(time()) . "," .
				" $this->formModIp=" . $dbconn->sqlString($_SERVER['REMOTE_ADDR']);
		}
	    
		
	//**************************************************************************
	// verifica che un form soddisfi i requisiti di obbligatorietà, 
	// altrimenti interrompe l'operazione di salvataggio con un messaggio
	function checkMandatory(waLibs\waForm $form)
		{
		if (!$form->checkMandatory())
			{
			$this->showMandatoryError();
			}
		}

	//*****************************************************************************
	function checkLockViolation(waLibs\waForm $form)
		{
		if ($form->record->value($this->formModId) != $form->getModId())
			{
			$this->showMessage("Errore violazione lock", "Errore di violazione del lock del record: il record e'" .
					" stato modificato da un altro operatore tra il momento " .
					" della tua lettura e il momento della tua scrittura." .
					" Sei pregato di ricaricare la pagina di modifica e" .
					" ripetere le modifiche che hai effettuato");
			}
		}

	//***************************************************************************
	/**
	* 
	* @return string
	*/
	function url_get_contents($url)
		{
		
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		$buffer = curl_exec($curl);
		curl_close($curl);		
		
		return $buffer;
		}

	//***************************************************************************
	/**
	 * restituisce il protocllo di connessione (http/s)
	*/
	function getProtocol()
		{
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";

		return $protocol;
		}
		
	//*****************************************************************************
	// logga le scritture del database su file sequenziale; questa funzione e'
	// la callback per il DB driver (vedi parametro $WADB_LOG_CALLBACK_FNC in
	// dbconfig.inc.php
	/**
	 * @todo attenzione!
	 * 
	 * @param type $sql
	 * @return type
	 */
	function logDBWrite($sql)
		{
		if (!defined("APPL_NOME_FILE_LOG_DB") || !APPL_NOME_FILE_LOG_DB)
			return;
			
		$record = array(
						date("Y-m-d H:i:s"),
						$this->user->IDUtente,
						$this->user->EMail ,
						$_SERVER['REMOTE_ADDR'],
						$_SERVER['PHP_SELF'],
						$sql
					);
		
		$fp = fopen(APPL_NOME_FILE_LOG_DB . "." . date("Ymd"), "a");
		fputcsv($fp, $record);
		fclose($fp);
		}
		
	//***************************************************************************** 
	/**
	 * non usato
	 * 
	 * @param waLibs\waDBConnection $dbconn
	 * @param type $out
	 */
	function logAccess(waLibs\waDBConnection $dbconn = null, $out = 0)
		{
		$dbconn = $dbconn ? $dbconn : $this->getDBConnection();
		$sql = "INSERT INTO access (id_user, section, flag_out, inserted, ip) VALUES (" .
					$dbconn->sqlInteger($this->user->IDUtente) . "," .
					$dbconn->sqlInteger($out) . "," .
					$dbconn->sqlDateTime(time()) . "," .
					$dbconn->sqlString($_SERVER['REMOTE_ADDR']) . ")";
		$this->dbExecute($sql, $dbconn);
		
		}

	//***************************************************************************
	/**
	* @return void
	*/
	function getMenu()
		{
		if ($this->childWindow)
			{
			return false;
			}

		$m = new waLibs\waMenu();
		$m->open();

		$m->openSection("Movimenti", "$this->httpwd/tblmovimenti.php");
		$m->closeSection();
		$m->openSection("Utenti", "$this->httpwd/tblutenti.php");
		$m->closeSection();
		$m->openSection("Strumenti");
			$m->addItem("Profilo", "javascript:document.waPage.openPage(\"$this->httpwd/formprofilo.php\")");
			$m->addItem("Preferenze", "javascript:document.waPage.openPage(\"$this->httpwd/formpreferenze.php\")");
			
			if ($this->user->Capofila)
				{
				$m->addItem("Configurazione cassa", "javascript:document.waPage.openPage(\"$this->httpwd/formconfig.php\")");
				$m->addItem("Cancellazione cassa", "javascript:document.waPage.openPage(\"$this->httpwd/formelimina_cassa.php\")");
				}
				
			$m->addItem("Logout", "$this->httpwd/logout.php");
		$m->closeSection();
			
		$m->close();

		include_once __DIR__ . "/ui/view/wamenu/wamenu.php";
		$m->view = new \webcash\wamenu_view();
		
		return $m;
		}
		
	//*****************************************************************************
	// verifica l'user loggato
	//*****************************************************************************
	/**
	 * 
	 * @param boolean $checkUser se l'user deve essere vwrificato
	 */
	function checkUser($checkUser, $checkAdmin)
		{
		if ($checkUser && !$this->user)
			{
			$this->redirect($this->loginPage);
			}
		
		if ($checkAdmin && !$this->user->Capofila)
			{
			$this->showMessage("Operazione non permessa", "Operazione non permessa", false);
			}
		
		}
		
	//*****************************************************************************
	// crea il blocco dei bottoni standard di una form
	//*****************************************************************************
	function form_submitButtons(waLibs\waForm $form, 
									$readOnly = false, 
									$showDeleteButton = true,
									$cmdOkCaption = 'Registra', 
									$cmdCancelCaption = 'Annulla', 
									$cmdDeleteCaption = 'Elimina',
									$cmdCloseCaption = 'Chiudi')
		{
		if (!$readOnly)
			{
			$okCtrl = new waLibs\waButton($form, 'cmd_submit', $cmdOkCaption);
			}
		if ($readOnly)
			{
			$cancelCtrl = new waLibs\waButton($form, 'cmd_cancel', $cmdCloseCaption);
			}
		else
			{
			$cancelCtrl = new waLibs\waButton($form, 'cmd_cancel', $cmdCancelCaption);
			}
		$cancelCtrl->cancel = true;
		$cancelCtrl->submit = false;
	
		if ($showDeleteButton && ! $readOnly)
			{
			$ctrl = new waLibs\waButton($form, 'cmd_delete', $cmdDeleteCaption);
			$ctrl->delete = true;
			}
			
		$ctrl = new waLibs\waButton($form, 'cmd_help', 'Help');
		$ctrl->submit = false;
		}
			
	//***************************************************************************
	/**
	* -
	*/
	function getPageTitle()
		{
		return $this->items["title"] ? $this->items["title"]->value : "";
		}
		
	//***************************************************************************
	/**
	* -
	* @return waLibs\waTable
	*/
	function getTable($sqlOrArray)
		{
		$table = new waLibs\waTable($sqlOrArray, $this->fileConfigDB);
		$viewType = $this->userPreferences["table_actions"];
		
		include_once __DIR__ . "/ui/view/watable/$viewType.php";
		$class = "\\webcash\\$viewType" . "_view";
		$table->view = new $class();
		
		// determinazione del nr di righe massimo della table
		if (!is_array($sqlOrArray))
			{
			if ($_GET['watable_no_pagination'])
				{
				// anche se chiedono di vedere tutto in  una sola pagina, limitiamo 
				// comunque la vista a 1000 righe, altrimenti rischiano di 
				// impallare il server
				$table->listMaxRec = 1000;
				$table->addAction("pagination", false, "Pagine");
				}
			else
				{
				$table->listMaxRec = $this->userPreferences["max_table_rows"] ? 
										$this->userPreferences["max_table_rows"] : 
										waLibs\waTable::LIST_MAX_REC;
				$table->addAction("no_pagination", false, "Tutti");
				}
			}
			
		$table->title = $this->getPageTitle();
		$table->removeAction("Details");
		$table->actions["New"]->label = "Nuovo";
		$table->actions["Edit"]->label = "Modifica";
		$table->actions["Delete"]->label = "Elimina";
		$table->actions["Filter"]->label = "Filtro";
		
		if ($this->childWindow)
			{
			$table->addAction("Close")->label = "Chiudi";
			// portiamo il bottone "chiudi in prima posizione
			$this->moveTableActionToTop($table, "Close");
			}
			
		$table->pdfOrientation = "L";
		
		return $table;
		}
		
	//***************************************************************************
	/**
	* sposta un'azione prima delle altre
	* 
	*/
	function moveTableActionToTop(waLibs\waTable $table, $actionName)
		{
		$swappo[$actionName] = $table->actions[$actionName];
		foreach ($table->actions as $k => $v)
			{
			if ($k != $actionName)
				{
				$swappo[$k] = $v;
				}
			}
		$table->actions = $swappo;
		
		}
		
	//***************************************************************************
	/**
	* -
	* @return waLibs\waForm
	*/
	function getForm($destinationPage = null)
		{
		$form = new waLibs\waForm($destinationPage, $this);
		include_once __DIR__ . "/ui/view/waform/waform.php";
		$form->view = new \webcash\waFormView();
		$form->fieldNameModId = $this->formModId;
		
		return $form;
		}
		
	//***************************************************************************
	/**
	* manda in output la pagina
	* 
	*/
	function show()
		{
		$version_data = (object) array("nr" => $this->version, "date" => date("Y-m-d", $this->versionDate));
		$this->addItem($version_data, "version_data");
			
		if (!$this->view)
			{
			include_once __DIR__ . "/ui/view/webcash.php";
			$this->view = new \webcash\waapplication_view();
			}
			
		waLibs\waApplication::show();
		exit();
		}
		
	//*************************************************************************
	// definisce l'url del documento all'interno di un controllo waUpload
	function setUrlDoc(waLibs\waUpload $ctrl)
		{
		$record = $ctrl->form->recordset->records[0];
		if ($record && $record->value($ctrl->name))
			{
			$ctrl->showPage =  $this->getUrlDoc($record, $ctrl->name);
			}
		}
		
	//***************************************************************************** 
	// elimina eventuali documenti salvati inprecedenza
	//***************************************************************************** 
	function deleteDoc(waLibs\waUpload $ctrl)
		{
		$rs = $ctrl->form->recordset;
		$id = $rs->records[0] ? $rs->records[0]->value(0) : $rs->dbConnection->lastInsertedId();
		$pattern = "$this->directoryDoc/" . $rs->tableName(0) . "/$ctrl->name/$id.*";
		$files = glob($pattern);
		if ($files)
			{
			foreach ($files as $file)
				{
				@unlink($file);
				}
			}
		}
		
	//***************************************************************************** 
	// salva l'eventuale documento allegato
	//***************************************************************************** 
	function saveDoc(waLibs\waUpload $ctrl)
		{
		// salvataggio del documento (se c'e'...)
		if ($ctrl->isToDelete())
			{
			// cancelliamo un eventuale documento esistente
			$this->deleteDoc($ctrl);
			}
		elseif ($ctrl->getUploadError())
			{
			$this->showMessage("Errore caricamento file", "Si e' verificato l'errore " .
					$ctrl->getUploadError() .
					" durante il caricamento del documento $ctrl->name." .
					" Si prega di avvertire l'assistenza tecnica.", false, true);
			}
		elseif ($ctrl->isToSave())
			{
			$this->deleteDoc($ctrl);
			$rs = $ctrl->form->recordset;
			$dest = "$this->directoryDoc/" . $rs->tableName(0);
			@mkdir($dest);
			$dest .= "/$ctrl->name";
			@mkdir($dest);

			$id = $rs->records[0] ? $rs->records[0]->value(0) : $rs->dbConnection->lastInsertedId();
			$dest = "$dest/$id." . pathinfo($ctrl->inputValue, PATHINFO_EXTENSION);
			if (!$ctrl->saveFile($dest))
				{
				$this->showMessage("Errore spostamento file", "Si e' verificato un errore " .
						" durante lo spostamento del documento $ctrl->name." .
						" Si prega di avvertire l'assistenza tecnica.", false, true);
				}
			}

		return true;			
		}
	
	//***************************************************************************
	/**
	* -
	*/
	function getTitleContext($table, $foreign_key, $description_field_name = "nome")
		{
		/**
		 * @todo sistemare
		 */
		$key = $_GET[$foreign_key];
		if (!$key)
			{
			return "";
			}
		$dbconn = $this->getDBConnection();
		$sql = "select $description_field_name from $table where $foreign_key=" . $dbconn->sqlString($key);
		$record = $this->getRecordset($sql, $dbconn, 1)->records[0];
		if (!$record)
			{
			return "";
			}

		return "\n" . $record->value(0);
		
		}

	//*************************************************************************
	// ritorna l'url di un documento che ha una corrispondenza in una riga di db
	/**
	 * 
	 * @param waLibs\waRecord $record
	 * @param string $fieldName
	 * @param string $table se vuoto prennde la table della chiave primaria, altrimenti la table data (evidentemente fk)
	 * @param string $idName se vuoto prennde il nome della chiave primaria del record; altrimenti il nome della chiave data (evidentemente fk)
	 * @return string
	 */
	function getUrlDoc(waLibs\waRecord $record, $fieldName, $table = '', $idName = '')
		{
		if (!$record->$fieldName)
			{
			return;
			}
		
		$table = $table ? $table : $record->recordset->tableName(0);
		$recPK = $idName ? $record->value($idName) : $record->value(0);
		$ext = pathinfo($record->$fieldName, PATHINFO_EXTENSION);
		$fs_doc = "$this->directoryDoc/$table/$fieldName/$recPK.$ext";
		
		$params["t"] = $table;
		$params["c"] = $fieldName;
		$params["k"] = $recPK;
		$params["e"] = $ext;
		
		return $this->getProtocol() . 
				"://$this->domain$this->httpwd/downloaddoc.php" .
				"?p=" . base64_encode(serialize($params)) .
				"&t=" . @filemtime($fs_doc);
		}
		
	//*****************************************************************************
	function record2Object(waLibs\waRecord $record)
		{
		return (object) $this->record2Array($record);
		}
		
	//**************************************************************************
	// l'eliminazione standard di un record è logica, non fisica
	function deleteRecord(waLibs\waRecord $record, $end_script = true)
		{
		if (!$record)
			{
			return;
			}

		$record->insertValue("Sospeso", 1);
		$this->setEditorData($record);
		$this->saveRecordset($record->recordset);
		if ($end_script)
			{
			$this->response();
			}
		}

	//***************************************************************************
	// ritorna una chiave personale da scrivere sul record
	//***************************************************************************
	function getPassword()
		{
		$mt = microtime();
		$elems = explode(" ", microtime());
		$pwd = chr((substr($elems[1], -1) + ord('A'))) . substr($elems[0], 2, 4) . substr($elems[1], -3);
		$pwd = substr($pwd, 0, -1) . chr(substr($pwd, -1) + ord('l'));
		return $pwd;
		}
	//***************************************************************************
	function decryptPassword($pwd)
		{
		// in mysql: aes_decrypt(unhex(campo, chiave)
		$dec = @mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->pwdPwd, pack("H*" , $pwd), MCRYPT_MODE_ECB);
		$toret = rtrim($dec, ((ord(substr($dec, strlen($dec) - 1, 1)) >= 0 and ord(substr($dec, strlen($dec) - 1, 1 ) ) <= 16 ) ? chr(ord(substr($dec, strlen($dec ) - 1, 1))): null) );			
		
		return $toret;
		}

	//***************************************************************************
	function encryptPassword($pwd)
		{
		// in mysql: hex(aes_encrypt(campo, chiave)
		// MySQL Padding
		$pad_len = 16 - (strlen($pwd) % 16);
		$pwd = str_pad($pwd, (16 * (floor(strlen($pwd) / 16) + 1)), chr($pad_len));
		 
		mt_srand();
		$td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_ECB, '');
		mcrypt_generic_init($td, $this->pwdPwd, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_DEV_URANDOM));
		$encrypted = mcrypt_generic($td, $pwd);
		mcrypt_generic_deinit($td);
		
		$toret = '';
		for ($i = 0; $i < strlen($encrypted); $i += 1)
			$toret .= str_pad(dechex(ord(substr($encrypted, $i, 1))), 2, '0', STR_PAD_LEFT);
			
		return strtoupper($toret);
		}
		
	//***************************************************************************
	// ritorna un token
	//***************************************************************************
	function getToken()
		{
		return substr($this->encryptPassword($this->encryptPassword($this->getPassword())), 0, 64);
		}
			
	//***************************************************************************
	function checkLoginExists($Login, $IDUtente, waLibs\waDBConnection $dbconn = null, $showMsg = true)
		{
		$dbconn = $dbconn ? $dbconn : $this->getDBConnection();
		$sql = "select IDUtente from Utenti" .
				" where Login=" . $dbconn->sqlString($Login) .
				" and IDutente!=" . $dbconn->sqlInteger($IDUtente ? $IDUtente : 0) .
				" and not Sospeso";
		$other_user = $this->getRecordset($sql, $dbconn, 1)->records[0];

		if ($other_user && $showMsg)
			{
			$this->showMessage("Login Utente non disponibile", "Login Utente già presente in archivio; sei pregato di sceglierne un'altra");
			}

		return $other_user ? true : false;
		}
		
	//***************************************************************************
	//encodes the string. Returns an array with the
	//string as the first element and the initialization
	//vector as the second element
	function easy_crypt($string, $key)
		{
		srand();
		$iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		$string = mcrypt_encrypt(MCRYPT_BLOWFISH, $key,
		                        $string, MCRYPT_MODE_CBC, $iv);
		
		return array(base64_encode($string), base64_encode($iv));
		}
	
	//***************************************************************************
	//decodes a string
	//the first argument is an array as returned by easy_encrypt()
	function easy_decrypt($cyph_arr, $key)
		{
		$out = mcrypt_decrypt(MCRYPT_BLOWFISH, $key, base64_decode($cyph_arr[0]),
		                     MCRYPT_MODE_CBC, base64_decode($cyph_arr[1]));
		
		return trim($out);
		}
	
	//*****************************************************************************
	function string2tmpfile($string, $name = '')
		{
		if (empty($name))
			{
			$name = tempnam(realpath($this->directoryTmp), rand());
			}
		$fp = fopen($name, "wb");
		if (!$fp)
			{
			return false;
			}
		fwrite($fp, $string);
		fclose($fp);
		return $name;
		}
		
	//***************************************************************************
	function saveEncryptString($string)
		{
		// generiamo la chiave
		$key = $this->getPassword();
		// encription della stringa; il risultato e' un array che serializziamo
		$tosave = serialize($this->easy_crypt($string, $key));
		// salviamo la string encryptata in un file
		$filename = $this->string2tmpfile($tosave);
		if (!$filename)
			{
			return false;
			}
		return array($key, $filename);
		
		}
	
	//***************************************************************************
	function getEncryptString($key, $filename)
		{
		$filename = realpath("$this->directoryTmp/$filename");
		if (!is_readable($filename))
			{
			return false;
			}
		$buffer = file_get_contents($filename);
		$cyph_arr = unserialize($buffer);
		unlink($filename);
		return $this->easy_decrypt($cyph_arr, $key);
		
		}
		
	//***************************************************************************
	function sendMailPassword(waLibs\waRecord $record)
		{
		$crlf = "\r\n";
		$body = "Ciao $record->Nome,$crlf$crlf" . 
				"queste sono le credenziali mediante cui potrai accedere a " . $this->title . ":$crlf$crlf" .
				
				"Login Utente: $record->Login$crlf" . 
				"Password:     $record->Password$crlf$crlf" .
				
				"Potrai modificare la tua password all'interno dell'applicazione, accedendo" .
				" al tuo profilo.$crlf$crlf".
				
				"Questa mail si genera automaticamente, non rispondere o scrivere" .
				" a questo indirizzo mail.$crlf$crlf" .
				
				"Ciao,$crlf$crlf" . 
				
				"lo staff tecnico $crlf" .
				$this->title . " $crlf";
		
		if (!$this->sendMail($record->EMail, "Credenziali accesso $this->title", $body))
			{
			$this->showMessage("Errore invio email", "Attenzione: le modifiche sono state registrate, ma si e' verificato" .
					" un errore durante l'invio del messaggio email contenente le credenziali di accesso.<p>" .
					"Sei pregato di avvisare l'assistenza tecnica e ripetere l'operazione" .
					" quando l'inconveniente sara' stato risolto.", false, true);
			}
		}
	
	//**************************************************************************
	} 	// fine classe webcash
	
//***************************************************************************

