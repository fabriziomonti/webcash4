<?php
include "webcash.inc.php";

//*****************************************************************************
class page extends webcash
	{
	/**
	 *
	 * @var waLibs\waForm
	 */
	var $form;
		
		
	//**************************************************************************
	function __construct()
		{
		parent::__construct(true, true);
		
		$this->createForm();

		if ($this->form->isToUpdate())
			{
			$this->updateRecord();
			}
		else
			{
			$this->showPage();
			}
		}

	//*****************************************************************************
	/**
	* mostra
	* 
	* costruisce la pagina contenente il form e la manda in output
	* @return void
	*/
	function showPage()
		{
		$this->addItem("Cancellazione cassa", "title");
		$this->addItem($this->form);
		$this->show();
			
		}
		
	//***************************************************************************
	function createForm()
		{
		
		$this->form = $this->getForm();
		$this->form->recordset = $this->getMyRecordset();
		
		$this->form->addBoolean("conferma", "Confermi cancellazione cassa?")->dbBound = false;

		$this->form_submitButtons($this->form, false, false);
		
		$this->form->getInputValues();

		}

	//***************************************************************************
	/**
	* -
	*
	* @return waLibs\waRecordset
	*/
	function getMyRecordset()
		{
		$dbconn = $this->getDBConnection();
		$sql = "select Casse.*" .
				" from Casse" .
				" where Casse.IDCassa=" . $dbconn->sqlInteger($this->user->IDCassa);
			
		$recordset = $this->getRecordset($sql, $dbconn, 1);
		if (!$recordset->records)
			{
			$this->showMessage("Item not found", "Item not found", false, true);
			}

		return $recordset;
		}
		
	//***************************************************************************
	function updateRecord()
		{
		$this->checkMandatory($this->form);

		if ($this->form->conferma)
			{
			$record = $this->form->recordset->records[0];
			$record->Sospeso = 1;	
			$this->setEditorData($record);
			$this->saveRecordset($record->recordset);
			$this->sessionData = array();
			}
		
		$this->response();
		}
		
	//*****************************************************************************
	}
		
		
//*****************************************************************************
// istanzia la pagina
new page();
