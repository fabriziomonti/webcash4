<?php
if (!defined('__VERSIONCONFIG_VARS'))
{
	define('__VERSIONCONFIG_VARS',1);
	
	define('APPL_REL', 							'4.0.4');
	define('APPL_REL_DATE', 					mktime(0,0,0, 12, 28, 2017));

	
} //  if (!defined('__VERSIONCONFIG_VARS'))
